<?php
class ApiService
{
    public function loadModel($name)
    {
        $path = 'models/' . $name . '_model.php';
        if (file_exists($path)) {
            require_once 'models/' . $name . '_model.php';
            $modelName = $name . '_Model';
            $this->model = new $modelName();
        }
    }

    public function GetAllRewards()
    {
        $this->loadModel("main");
        // $postdata = file_get_contents("php://input");
        $this->model->GetAllRewards();
    }

    public function GetPersonByID()
    {
        $this->loadModel("main");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        // echo $request->perid;
        $this->model->GetPersonByID($request->perid);
    }

    public function UpdateRewardAmount()
    {
        $this->loadModel("main");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        // echo $request->perid;
        $this->model->UpdateRewardAmount($request);
    }

    public function CheckAwardStatus()
    {
        $this->loadModel("main");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        // echo $request->perid;
        $this->model->CheckAwardStatus($request->perid);
    }

    public function UpdatePersonReward()
    {
        $this->loadModel("main");
        $postdata = file_get_contents("php://input");
        $request = json_decode($postdata);
        // echo $request->perid;
        $this->model->UpdatePersonReward($request);
    }
}
