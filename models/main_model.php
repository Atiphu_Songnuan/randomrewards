<?php
class Main_Model extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function GetPersonByID($perid)
    {
        $sql = "SELECT PERID, NAME, SURNAME, Dep_name FROM viewpersonindepart WHERE  PERID=$perid";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        echo $jsonData;
    }

    public function GetAllRewards()
    {
        // echo json_encode($perid);
        // $encodePassword = md5($password);
        $sql = "SELECT REWARD_ID, REWARD_NAME, REWARD_AMOUNT FROM tbl_reward";
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        echo $jsonData;
    }

    public function UpdateRewardAmount($rewarddata)
    {
        if (isset($rewarddata)) {
            $rewardid = $rewarddata->reward_id;
            $newamount = $rewarddata->newamount;

            $data = [
                'rewardid' => $rewardid,
                'newamount' => $newamount,
            ];

            $sql = "UPDATE tbl_reward SET REWARD_AMOUNT =:newamount WHERE REWARD_ID = :rewardid";
            $sth = $this->db->prepare($sql);
            $sth->execute($data);
            echo true;
        } else {
            echo false;
        }
    }

    public function CheckAwardStatus($perid)
    {
        $sql = "SELECT PERID, JOIN_PARTY, AWARD, AWARD_NAME, OT FROM tbl_register WHERE PERID=$perid";
        // $sql = 'SELECT PERID, JOIN_PARTY, AWARD, AWARD_NAME FROM tbl_register WHERE PERID='.$perid.'';
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll();
        $jsonData = json_encode($data);
        echo $jsonData;
    }

    public function UpdatePersonReward($personrewarddata)
    {
        if (isset($personrewarddata)) {
            $perid = $personrewarddata->perid;
            $rewardid = $personrewarddata->reward_id;
            $rewardname = $personrewarddata->reward_name;

            $data = [
                'perid' => $perid,
                'rewardid' => $rewardid,
                'rewardname' => $rewardname
            ];

            $sql = "UPDATE tbl_register
                    SET AWARD =:rewardid, 
                        AWARD_NAME = :rewardname,
                        JOIN_PARTY = CASE
                                     WHEN JOIN_PARTY IS NULL THEN 'Y'
                                     ELSE JOIN_PARTY
                                     END,
                        REGISTER_DATE_JOIN =    CASE
                                                WHEN REGISTER_DATE_JOIN IS NULL THEN NOW()
                                                ELSE REGISTER_DATE_JOIN
                                                END,
                        REGISTER_TIME_JOIN =    CASE
                                                WHEN REGISTER_TIME_JOIN IS NULL THEN NOW()
                                                ELSE REGISTER_TIME_JOIN
                                                END
                    WHERE PERID = :perid";
            $sth = $this->db->prepare($sql);
            $sth->execute($data);
            echo true;
        } else {
            echo false;
        }
    }
}
