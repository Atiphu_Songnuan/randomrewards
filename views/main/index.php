<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>เกมส์สุ่มรางวัล</title>
    
    <link rel="icon" type="image/png" href="./public/images/icons/dice.ico"/> 

    <link href="./public/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="./public/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
 
    <!-- SB Admin 2 -->
    <link href="./public/css/sb-admin-2.min.css" rel="stylesheet">

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="./public/vendor/sweetalert2/dist/sweetalert2.min.css">

    <!-- Bootstrap -->
    <!-- <link rel="stylesheet" href="./public/vendor/bootstrap/dist/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kanit">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

    <style>
        body {
            /* background-image: linear-gradient(to top, #d5dee7 0%, #ffafbd 0%, #c9ffbf 100%); */

            /* background-image: linear-gradient(-20deg, #fc6076 0%, #ff9a44 100%); */
            background: #EF3B36;  /* fallback for old browsers */
            background: -webkit-linear-gradient(to bottom, #FFFFFF, #EF3B36);  /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to bottom, #FFFFFF, #EF3B36); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */


            font-family: 'Kanit', serif;
            font-size: 28px;
        }
        label{
            font-size: 22px;
        }
        h4{
            font-size: 24px;
        }
        h3{
            font-size: 28px;
        }
        .edit-modal .modal {
            position: relative;
            top: auto;
            bottom: auto;
            right: auto;
            left: auto;
            display: block;
            z-index: 1;
        }
        .edit-modal .modal {
            background: transparent !important;
        }
        .swal2-popup {
            font-size: 0.7rem !important;
        }
    </style>
</head>
<body class="content-wrapper">
    <!-- Require Header from header.php -->
    <?php require './views/header.php'?>
    <div class="container-fluid text-center">
        <div class="row">
            <!-- <div class="col-xl-12 col-md-12 mb-12 text-center mb-4">
                <label class="btn btn-success" style="box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); padding: 14px 40px;">
                    Scan QR Code
                    <input type=file accept="image/*" capture=environment onchange="OpenQRCamera(this);" id="btnuploadqrcode" hidden>
                    <span><i class="fa fa-qrcode"></i></span>
                </label>
            </div> -->

            <div class="col-xl-12 col-md-12 mb-12 text-center mb-4">
                <div class="input-group col-md-3 mx-auto">
                    <input id="inputperid" type="number" class="form-control" placeholder="รหัสบุคลากร" aria-label="Username" aria-describedby="basic-addon1" >
                    <div class="input-group-prepend">
                        <button id="btnsearch" class="btn btn-info" onclick="SearchPerson()"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>

            <div class="col-xl-12 col-md-12 mb-12 text-center">
                <img id="personimg" src="./public/images/search.svg" class='rounded-circle' width='120px' height='120px' onerror="this.onerror=null;this.src='./public/images/question.svg';" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);">
            </div>

            <div class="col-xl-12 col-md-12 mb-12 text-center">
                <label class="text-white " style="font-size: 18px; text-align: center;">รหัสบุคลากร: </label>
                <label id="perid" class="text-danger" style="font-size: 18px; text-align: center;">-</label>
            </div>

            <div class="col-xl-12 col-md-12 mb-12 text-center">
                <label class="text-white " style="font-size: 18px; text-align: center; ;">ชื่อ-นามสกุล: </label>
                <label id="name" class="text-danger" style="font-size: 18px; text-align: center;">-</label>
            </div>

            <div class="col-xl-12 col-md-12 mb-12 text-center mb-4">
                <label id="recievedreward" style="color:white; font-size: 18px; text-align: center;" hidden></label>
            </div>

            <div class="col-xl-12 col-md-12 mb-12 text-center mb-4">
                <img id="randimg" alt="number" width="200px" height="200px">
            </div>

            <div class="col-xl-12 col-md-12 mb-12 text-center mb-4">
                <button class="btn btn-primary" id="btnRandom" style="box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); padding: 14px 40px;" disabled>เริ่มสุ่ม</button>
                <button class="btn btn-danger" id="btnPause" style="box-shadow: 0 8px 8px 0 rgba(0,0,0,0.2), 0 8px 8px 0 rgba(0,0,0,0.19); padding: 14px 40px;" disabled>หยุด</button>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="./public/vendor/jquery/jquery.min.js"></script>
    <script src="./public/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="./public/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="./public/js/sb-admin-2.min.js"></script>

    <!-- SweetAlert2 -->
    <script src="./public/vendor/sweetalert2/dist/sweetalert2.min.js"></script>

    <script src="./public/dist/js/qr_packed.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/particlesjs/2.2.2/particles.min.js"></script>

    <script>
        var weight_list = [];
        var isPaused = false;
        var numberOfRewards;
        var interval = null ;

        // var perid = "";
        // function OpenQRCamera(node) {
            //     perid = "";
            //     var reader = new FileReader();
            //     reader.onload = function() {
            //         node.value = "";
            //         qrcode.callback = function(res) {
            //             // var parameters = res.split("?")[1].split('&');
            //             // // var perid = [];
            //             // parameters.forEach(function(element){
            //             //     perid.push(element.split("=")[1]);
            //             // });
            //             perid = res;
            //             if(res instanceof Error) {
            //                 // alert("No QR code found. Please make sure the QR code is within the camera's frame and try again.");
            //                 // alert("ไม่พบ QR Code. กรุณา Scan QR Code ใหม่อีกครั้ง.");
            //                 $('#inputperid').val('');
            //                 Swal.fire({
            //                     title: 'ไม่พบ QR Code. กรุณา Scan QR Code ใหม่อีกครั้ง.',
            //                     type: 'error',
            //                     timer: 3000,
            //                     showConfirmButton: true,
            //                 })
            //             } else {
            //                 // node.parentNode.previousElementSibling.value = res;                        
            //                 var jsondata = {"perid": perid};
            //                 GetPersonByID(jsondata);
            //             }
            //         };
            //         qrcode.decode(reader.result);
            //     };
            //     reader.readAsDataURL(node.files[0]);
        // }

        // Press Enter to Login
        $('body').keyup(function(e) {
            var code = e.keyCode || e.which;
            if (code == '13') {
                SearchPerson();
                $('#btnsearch').focus();
            }
        });

        function SearchPerson(){
            // console.log($('#inputperid').val());
            // perid = $('#inputperid').val();
            var jsondata = {"perid": $('#inputperid').val()}

            GetPersonByID(jsondata);

        }

        function GetPersonByID(jsondata){
            // console.log(jsondata)
            $.ajax({
                type:"POST",
                url:"../randomrewards/ApiService/GetPersonByID",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: JSON.stringify(jsondata),
                contentType: "application/json",
                dataType: "json",
                success: function(response) {
                    // console.log(response);
                    //***แปลง string array ให้เป็น json แล้วสร้างเป็น objects จากนั้นทำการสั่งให้ set multiselect***/
                    // console.log(response[0]['PERID']);
                    //ตรวจสอบข้อมูลของบุคลากร ว่ามีรหัส, ชื่อ, นามสกุล ครบหรือไม่
                    var persondataNotEmpty = false;
                    if (response.length != 0) {
                        persondataNotEmpty = true;
                    } else{
                        persondataNotEmpty = false;
                    }

                    if (persondataNotEmpty) {
                        $('#personimg').attr('src', 'http://mednet.psu.ac.th/Personal/ShowfPict.php?id='+ response[0]['PERID']);
                        $('#personimg').attr('onerror', "this.onerror=null;this.src='./public/images/search.svg'");
                        $('#perid').text(response[0]['PERID']).attr('class', 'text-primary');
                        $('#name').text(response[0]['NAME'] + " " + response[0]['SURNAME']).attr('class', 'text-primary');

                        CheckStatus(jsondata, response);
                    } else{
                        // $('#personimg').attr('hidden', true);
                        $('#personimg').attr('src', './public/images/search.svg');
                        $('#personimg').attr('onerror', "this.onerror=null;this.src='./public/images/search.svg'");

                        $('#perid').text('ไม่พบข้อมูล').attr('class', 'animated infinite heartBeat slow text-danger');
                        $('#name').text('ไม่พบข้อมูล').attr('class', 'animated infinite heartBeat slow text-danger');
                        $('#recievedreward').text('').attr('hidden', true);
                        Swal.fire({
                            title: 'ไม่พบข้อมูลบุคลากร กรุณาตรวจสอบข้อมูลของท่านก่อน!',
                            type: 'error',
                            timer: 3000,
                            showConfirmButton: false,
                        })
                        $('#btnRandom').attr("disabled", true);
                        $('#btnPause').attr("disabled", true);

                    }
                },
                error: function(response) {
                    console.log(response.responseText);
                }
            });
        }

        function CheckStatus(jsondata, response){
            // console.log(jsondata);
            // console.log(response);
            $.ajax({
                type:"POST",
                url:"../randomrewards/ApiService/CheckAwardStatus",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: JSON.stringify(jsondata),
                contentType: "application/json",
                dataType: "json",
                success: function(checkawardresponse) {
                    // console.log(checkawardresponse);
                    //หากมีข้อมูลครบถ้วน ทำการตรวจสอบสิทธิ์ในการสอยดาว ่วาได้ทำการใช้สิทธิ์ไปแล้วหรือยัง
                    //True สามารถสอยดาวได้
                    //False ไม่สามารถสอยดาวได้ แล้วทำการแจ้งของรางวัลที่ได้รับให้ทราบ
                    if (checkawardresponse.length != 0) {
                        if (checkawardresponse[0]['OT'] == 'Y') {
                            $('#recievedreward').text('').attr('hidden', true);
                            $('#btnRandom').attr("disabled", true);
                            Swal.fire({
                                title: 'ไม่สามารถสุ่มรางวัลได้',
                                text: 'เนื่องจากคุณอยู่เวร',
                                type: 'success',
                                timer: 3000,
                                showConfirmButton: false,
                            });
                        }else{
                            if ((checkawardresponse[0]['AWARD'] != null && checkawardresponse[0]['AWARD_NAME'] != null) && (checkawardresponse[0]['AWARD'] != '' && checkawardresponse[0]['AWARD_NAME'] != '')) {
                                $('#btnRandom').attr("disabled", true);
                                $('#recievedreward').text('').attr('hidden', false);
                                $('#recievedreward').text('ได้รับรางวัลแล้ว!').attr('class', 'animated infinite heartBeat slow text-danger');
                                $('#recievedreward').text('ได้รับรางวัลแล้ว!').attr('style', 'font-weight: bold;');
                                var imagename = SetImageName(checkawardresponse[0]['AWARD_NAME']);
                                var awardnumber = SetAwardNumber(checkawardresponse[0]['AWARD']);
                                Swal.fire({
                                    // title: 'คุณ'+ response[0]['NAME'] + " " + response[0]['SURNAME'] + " ได้รับของรางวัลแล้ว",
                                    // text: 'รางวัลของคุณคือ "' + checkawardresponse[0]['AWARD_NAME'],

                                    // imageUrl: "./public/images/rewards/"+ imagename +".png",
                                    html: "<label class='text-primary'>คุณ"+response[0]['NAME'] + " " + response[0]['SURNAME']+"</label>"+
                                          "<br>"+
                                          "<label>ได้รับรางวัลหมายเลข</label>"+
                                          "<br>"+
                                          "<img id='personimg' src='./public/images/number/"+awardnumber+".svg' class='rounded-circle mb-4' width='60px' height='60px'>"+
                                          "<br>"+
                                          "<img id='personimg' src='./public/images/rewards/"+imagename+".png' class='mb-4 'width='200px' height='200px'>" +
                                          "<br>"+
                                          "<label style='font-size: 18px'>"+checkawardresponse[0]['AWARD_NAME']+"</label>",
                                    // imageUrl: "./public/images/number/"+ awardnumber +".svg",
                                    imageWidth: 200,
                                    imageHeight: 200,
                                    // type: 'info',
                                    showConfirmButton: true,
                                })
                            } else{
                                $('#reward').text('');
                                $('#recievedreward').text('').attr('hidden', true);
                                // Swal.fire({
                                //     title: 'คุณ'+ response[0]['NAME'] + " " + response[0]['SURNAME'],
                                //     text: 'มี 1 สิทธิ์ในการสุ่มรางวัล',
                                //     type: 'success',
                                //     timer: 3000,
                                //     showConfirmButton: false,
                                // });
                                // $('#btnRandom').attr("disabled", false);
                                var rewardData;
                                $.ajax({
                                    type:"POST",
                                    url:"../randomrewards/ApiService/GetAllRewards",
                                    headers: {
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    },
                                    data: JSON.stringify(jsondata),
                                    contentType: "application/json",
                                    dataType: "json",
                                    success: function(rewardresponse) {
                                        // console.log(rewardresponse);
                                        
                                        rewardData = rewardresponse;
                                        
                                        var list = [];
                                        var amount = [];
                                        var sum = 0;
                                        var weight = [];
                                        var rewardJson = [];

                                        weight_list = [];
                                        rewardData.forEach(function(element){
                                            rewardJson.push({"reward_id": element['REWARD_ID'],
                                                            "reward_name": element['REWARD_NAME'],
                                                            "reward_amount": element['REWARD_AMOUNT']});
                                        });

                                        for (let index = 0; index < rewardJson.length; index++) {
                                            sum += parseInt(rewardJson[index]['reward_amount']);
                                        }
                                        // console.log("Reward Amount: " + sum);
                                        
                                        for (let index = 0; index < rewardJson.length; index++) {
                                            var average = parseFloat(((rewardJson[index]['reward_amount'] * 100)/sum)/100).toFixed(5);
                                            weight.push(average);
                                        }
                                        // console.log(weight);
                                        // Loop over weights
                                        for (var i = 0; i < weight.length; i++) {
                                            var multiples = weight[i] * 100;                
                                            // Loop over the list of items
                                            for (var j = 0; j < multiples; j++) {
                                                weight_list.push(rewardJson[i]);
                                            }
                                        }
                                        // console.log(weight_list);

                                        // เช็คว่าของหมดหรือยัง ถ้าหมดแล้ว "ห้ามสุ่ม"
                                        if (weight_list.length == 0) {
                                            $('#btnRandom').attr('disabled', true);
                                            Swal.fire({
                                                title: 'ของรางวัลหมดแล้วไม่สามารถสุ่มรางวัลได้',
                                                type: 'warning',
                                                timer: 3000,
                                                showConfirmButton: false,
                                            });
                                        } else{
                                            $('#btnRandom').attr("disabled", false);
                                            Swal.fire({
                                                title: 'คุณ'+ response[0]['NAME'] + " " + response[0]['SURNAME'],
                                                text: 'มี 1 สิทธิ์ในการสุ่มรางวัล',
                                                type: 'success',
                                                timer: 3000,
                                                showConfirmButton: false,
                                            });
                                        }
                                    },
                                    error: function(rewardresponse){
                                        console.log(rewardresponse);
                                    }
                                });
                            }
                        }
                    } else{
                        $('#btnRandom').attr('disabled', true);
                        $('#recievedreward').text('').attr('hidden', true);
                        Swal.fire({
                            title: 'คุณ'+ response[0]['NAME'] + " " + response[0]['SURNAME'] +' กรุณาลงทะเบียนเข้างานเพื่อรับสิทธิ์',
                            type: 'warning',
                            timer: 3000,
                            showConfirmButton: false,
                        })
                    }
                },
                error: function(checkawardresponse){
                    console.log(checkawardresponse);
                }
            });
        }

        $('#randimg').attr("src", "./public/images/number/one.svg")

        $('#btnRandom').on('click', function(e) {
            $('#btnPause').attr("disabled", false);
            e.preventDefault();
            isPaused = false;
            var interval = window.setInterval(function(){
                if (!isPaused) {
                    var rand = Math.floor(Math.random()*weight_list.length);
                    numberOfRewards = rand;
                    var imagename = SetImageName(weight_list[rand]['reward_name']);
                    $('#randimg').attr("src", "./public/images/number/"+ imagename +".svg");
                }
            }, 1);
        });

        $('#btnPause').on('click', function(e) {
            $('#btnRandom').attr("disabled", true);
            $('#btnPause').attr("disabled", true);
            // console.log("Reward ID: " + weight_list[numberOfRewards]['reward_id']);
            var newamount = parseInt(weight_list[numberOfRewards]['reward_amount'])-1;
            var jsondata = {"reward_id": weight_list[numberOfRewards]['reward_id'],
                            "newamount": newamount};
            $.ajax({
                type:"POST",
                url:"../randomrewards/ApiService/UpdateRewardAmount",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: JSON.stringify(jsondata),
                contentType: "application/json",
                dataType: "json",
                success: function(response) {
                    // console.log(response);
                    //***แปลง string array ให้เป็น json แล้วสร้างเป็น objects จากนั้นทำการสั่งให้ set multiselect***/
                    var imagename = SetImageName(weight_list[numberOfRewards]['reward_name']);
                    var awardnumber = SetAwardNumber(weight_list[numberOfRewards]['reward_id']);
                    Swal.fire({
                        html:   "<label class='text-primary'>ยินดีด้วย! คุณ"+ $('#name').text() +"</label>"+
                                "<br>"+
                                "<label>ได้รับรางวัลหมายเลข</label>"+
                                "<br>"+
                                "<img id='personimg' src='./public/images/number/"+awardnumber+".svg' class='rounded-circle mb-4' width='60px' height='60px'>"+
                                "<br>"+
                                "<img id='personimg' src='./public/images/rewards/"+imagename+".png' class='mb-4 'width='200px' height='200px'>" +
                                "<br>"+
                                "<label style='font-size: 18px'>"+ weight_list[numberOfRewards]['reward_name'] +"</label>",
                    }).then((result) =>{
                        $('#recievedreward').text('').attr('hidden', false);
                        $('#recievedreward').text('ได้รับรางวัลแล้ว!').attr('class', 'animated infinite heartBeat fast text-danger');
                        $('#recievedreward').text('ได้รับรางวัลแล้ว!').attr('style', 'font-weight: bold;'); 
                    });
                    // //Insert ข้อมูลรางวัลให้ผู้ที่ได้รับรางวัล
                    var jsondata = {"perid": $('#inputperid').val(),
                                    "reward_id": weight_list[numberOfRewards]['reward_id'],
                                    "reward_name": weight_list[numberOfRewards]['reward_name']};
                    // console.log(jsondata);
                    $.ajax({
                        type:"POST",
                        url:"../randomrewards/ApiService/UpdatePersonReward",
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                        data: JSON.stringify(jsondata),
                        contentType: "application/json",
                        dataType: "json",
                        success: function(response) {
                            // console.log(response);
                            // if (response[0]['REGISTER_DATE_JOIN'] != null && response[0]['REGISTER_DATE_JOIN']) {
                                
                            // }
                        },
                        error: function(response){
                            console.log(response);
                        }
                    });
                },
                error: function(response) {
                    console.log(response);
                }
            });
            e.preventDefault();
            isPaused = true;
        });

        function SetImageName(imagename){
            var img = "";
            switch (imagename) {
                case "จักรยาน":
                    img = "one";
                    break;
                case "นาฬิกา":
                    img = "two";
                    break;
                case "เครื่องคั้นน้ำผลไม้":
                    img = "three";
                    break;
                case "ปิ่นโต":
                    img = "four";
                    break;
                case "แก้วน้ำ":
                    img = "five";
                    break;
                case "กระเป๋าคาดเอว (ออกกำลังกาย)":
                    img = "six";
                    break;
                case "ข้าวสาร (ไรซ์เบอร์รี่)":
                    img = "seven";
                    break;
                case "ถุงผ้าพับเก็บได้":
                    img = "eight";
                    break;
                default:
                    break;
            }
            return img;
        }

        function SetAwardNumber(awardnumber){
            var img = "";
            switch (awardnumber) {
                case "1":
                    img = "one";
                    break;
                case "2":
                    img = "two";
                    break;
                case "3":
                    img = "three";
                    break;
                case "4":
                    img = "four";
                    break;
                case "5":
                    img = "five";
                    break;
                case "6":
                    img = "six";
                    break;
                case "7":
                    img = "seven";
                    break;
                case "8":
                    img = "eight";
                    break;
                default:
                    break;
            }
            return img;
        }

    </script>
</body>
</html>